from django.test import TestCase, Client
from .models import *
from datetime import datetime
import pytz

class TestMaps(TestCase):
    def setUp(self):
        Report.objects.create(age=35, gender='M', status='Recovered', province='Jawa Barat', city='Bogor')
        Report.objects.create(age=20, gender='L', status='Showing Symptoms', province='DKI Jakarta', city='Jakarta Selatan')

    def test_add_report(self):
        count = Report.objects.all().count()
        Report.objects.create(age=20, gender='L', status='Positive', province='Jawa Barat', city='Bandung')
        self.assertEqual(Report.objects.all().count(), count+1)

    def test_model_name(self):
        report = Report.objects.all()[0]
        self.assertEqual(str(report), report.dt.strftime('%b %d, %Y @ %H:%M'))

    def test_add_report_from_page(self):
        count = Report.objects.all().count()
        response = Client().post('/maps/report/', data={'age':19, 'gender':{'choices': ('M')}, 'status':{'choices': ('Showing Symptoms')}, 'province':{'choices': ('Jawa Barat')}, 'city':'Depok'})
        self.assertEqual(Report.objects.all().count(), count+1)
        self.assertEqual(response.status_code, 302)

    def test_url_maps_exist(self):
        response = Client().get('/maps/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'maps.html')

    def test_url_report_exist(self):
        response = Client().get('/maps/report/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'report.html')

    def test_url_reports_exist(self):
        response = Client().get('/maps/self-reports/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'reports.html')