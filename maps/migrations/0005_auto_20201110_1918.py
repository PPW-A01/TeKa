# Generated by Django 3.1.2 on 2020-11-10 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0004_auto_20201110_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='status',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='report',
            name='province',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
