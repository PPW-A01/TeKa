from django.urls import path

from . import views

app_name = 'maps'

urlpatterns = [
    path('', views.maps, name='maps'),
    path('report/', views.report, name='report'),
    path('self-reports/', views.reports, name='self-reports')
]