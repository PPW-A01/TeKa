from django.shortcuts import render, redirect
from maps.forms import ReportForm
from maps.models import Report

def maps(request):
    data = Report.objects.all().order_by('id').reverse()[:10]
    context = {
        'data' : data,
    }
    return render(request, 'maps.html', context)

def report(request):
    form = ReportForm(request.POST or None)
    if form.is_valid and request.method == 'POST':
        form.save()
        return redirect('/maps/self-reports')
    context = {
        'form' : form,
    }
    return render(request, 'report.html', context)

def reports(request):
    data = Report.objects.all()
    context = {
        'data' : data,
    }
    return render(request, 'reports.html', context)