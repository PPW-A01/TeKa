from django.urls import path
from . import views
app_name = 'fitur'

urlpatterns = [
    path('tipsform/', views.tipsform, name= 'tips_form'),
    path('', views.tipslist, name= 'tips_list'),
]