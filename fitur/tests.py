from django.test import TestCase,Client
from django.urls import resolve
from django.http import HttpRequest, response

from .models import Fiturku
from .views import *

# Create your tests here.
class TestTips(TestCase):
    def setUp(self):
        Fiturku.objects.create(tips = "mencuci tangan")

    def test_url_tips_exist(self):
        response= Client().get('/tips/')
        self.assertEqual(response.status_code, 200)

    def test_url_form_exist(self):
        response = Client().get('/tips/tipsform/')
        self.assertEqual(response.status_code, 200)

    def test_memuat_model_fitur(self):
        fitur = Fiturku.objects.create(tips = "memakai masker")
        count = Fiturku.objects.all().count()
        self.assertEqual(count,2)

    def test_model_name(self):
        tips = Fiturku.objects.all()[0]
        self.assertEqual(str(tips), "mencuci tangan")

    def test_add_tips_from_page(self):
        count = Fiturku.objects.all().count()
        response = Client().post('/tips/tipsform/', data= {'tips': 'menjaga jarak'})
        self.assertEqual(Fiturku.objects.all().count(), count+1)
        self.assertEqual(response.status_code, 302)
