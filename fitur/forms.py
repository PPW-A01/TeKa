from django import forms
from .models import Fiturku

class Fiturform(forms.ModelForm):


    class Meta :
        model = Fiturku
        fields = [
            'tips'
        ]

        widgets = {
            'tips' : forms.Textarea(attrs={
                'class':'form-style mb-5',
                'rows' : 4,
                'cols' : 25,

                }),
        }