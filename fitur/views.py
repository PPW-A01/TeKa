from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404

from .forms import Fiturform
from .models import Fiturku

# Create your views here.
def tipslist(request):
    listp = Fiturku.objects.all()
    return render (request, 'fitur/tips_list.html', {'listp':listp})


def tipsform(request):
    form = Fiturform()
    if request.method == 'POST':
        form = Fiturform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/tips')

    return render (request, 'fitur/tips_form.html', {'form':form})