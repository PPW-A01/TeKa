from django.shortcuts import render, redirect
from .forms import FeedbackForm
from .models import Feedback

# Create your views here.
def feedback(request):
    form = FeedbackForm()
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('./collection')
    
    content = {
        'form' : form
    }
    return render(request, 'TeKa/feedback.html', content)

def feedbackCollection(request):
    data = Feedback.objects.all()
    content = {
        'data' : data
    }
    
    return render(request, 'TeKa/feedback-collection.html', content)

def deleteFeedback(request, pk):
    data = Feedback.objects.get(id = pk)
    if request.method == 'POST':
        data.delete()
        return redirect('../../collection')
    
    return render(request, 'TeKa/delete-feedback.html')
