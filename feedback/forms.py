from django import forms
from django.forms import ModelForm
from .models import Feedback

class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'
    
    email = forms.EmailField(
        required = True,
        max_length = 254,
        widget = forms.EmailInput(
            attrs = {
                'id' : 'user-email',
                'class' : 'form-control',
                'placeholder' : 'Please input your email here',
                'autocomplete' : 'off',
            }
        )
    )

    feedback_description = forms.CharField(
        required = True,
        max_length = 1000,
        widget = forms.Textarea(
            attrs = {
                'id' : 'user-feedback',
                'class' : 'form-control',
                'placeholder' : 'Put your opinion about this website here',
                'autocomplete' : 'off',
            }
        )
    )