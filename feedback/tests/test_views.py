from http import HTTPStatus as stats
from django.test import TestCase, Client
from django.urls import resolve, reverse
from feedback.models import Feedback
from feedback.forms import FeedbackForm

# Create your tests here.
class feedbackViewsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.test_feedback = Feedback.objects.create(
            email = 'kelompokppw.a01@protonmail.com',
            feedback_description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        )
        cls.feedback_url = '/feedback/'
        cls.feedbackCollection_url = '/feedback/collection/'
        cls.deleteFeedback_url = '/feedback/deleteFeedback/%d/' % cls.test_feedback.id

    def test_feedback_url_GET(self):
        response = self.client.get(self.feedback_url)
        self.assertTemplateUsed(response, 'TeKa/feedback.html')
        self.assertEqual(response.status_code, stats.OK)
    
    def test_feedback_collection_url_GET(self):
        response = self.client.get(self.feedbackCollection_url)
        self.assertTemplateUsed(response, 'TeKa/feedback-collection.html')
        self.assertEqual(response.status_code, stats.OK)
    
    def test_delete_feedback_url_GET(self):
        response = self.client.get(self.deleteFeedback_url)
        self.assertTemplateUsed(response, 'TeKa/delete-feedback.html')
        self.assertEqual(response.status_code, stats.OK)
    
    def test_feedback_url_POST(self):
        response = self.client.post(self.feedback_url, {
            'email' : 'kelompokppw.a01@protonmail.com',
            'feedback_description' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        }, follow = True)
        self.assertEqual(response.status_code, stats.OK)
    
    def test_delete_feedback_url_POST(self):
        response = self.client.post(self.deleteFeedback_url, follow = True)
        self.assertEqual(response.status_code, stats.OK)