from django.test import TestCase
from feedback.models import Feedback

# Create your tests here.
class feedbackModelsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_feedback = Feedback.objects.create(
            email = 'kelompokppw.a01@protonmail.com',
            feedback_description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        )
    
    def test_email_label(self):
        email_address = Feedback.objects.get(id = 1)
        field_label = email_address.email
        self.assertEqual(field_label, 'kelompokppw.a01@protonmail.com')
    
    def test_feedback_description_label(self):
        feedback_description_content = Feedback.objects.get(id = 1)
        field_label = feedback_description_content.feedback_description
        self.assertEqual(field_label, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    
    def test_feedback_email_max_length(self):
        email_address = Feedback.objects.get(id = 1)
        max_length= email_address._meta.get_field('email').max_length
        self.assertEqual(max_length, 254)
    
    def test_feedback_feedbackDescription_max_length(self):
        feedback_description_content = Feedback.objects.get(id = 1)
        max_length = feedback_description_content._meta.get_field('feedback_description').max_length
        self.assertEqual(max_length, 1000)
    
    def test_feedback_str_label(self):
        email_address = Feedback.objects.get(id = 1)
        expected_email_address = f'{email_address.email}'
        self.assertEqual(expected_email_address, str(email_address))