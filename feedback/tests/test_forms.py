from django.test import TestCase
from feedback.forms import FeedbackForm

# Create your tests here.
class feedbackFormsTest(TestCase):
    def test_email_attribute(self):
        email_component = FeedbackForm()
        email_attribute = email_component.fields['email']
        email_attribute_widget = email_attribute.widget
        email_required = email_attribute.required
        email_max_length = email_attribute.max_length
        email_id = email_attribute_widget.attrs['id']
        email_class = email_attribute_widget.attrs['class']
        email_placeholder = email_attribute_widget.attrs['placeholder']
        email_autocomplete = email_attribute_widget.attrs['autocomplete']
        self.assertTrue(email_required)
        self.assertEqual(email_max_length, 254)
        self.assertEqual(email_id, 'user-email')
        self.assertEqual(email_class, 'form-control')
        self.assertEqual(email_placeholder, 'Please input your email here')
        self.assertEqual(email_autocomplete, 'off')
    
    def test_feedback_description_attribute(self):
        description_component = FeedbackForm()
        description_attribute = description_component.fields['feedback_description']
        description_attribute_widget = description_attribute.widget
        description_required = description_attribute.required
        description_max_length = description_attribute.max_length
        description_id = description_attribute_widget.attrs['id']
        description_class = description_attribute_widget.attrs['class']
        description_placeholder = description_attribute_widget.attrs['placeholder']
        description_autocomplete = description_attribute_widget.attrs['autocomplete']
        self.assertTrue(description_required)
        self.assertEqual(description_max_length, 1000)
        self.assertEqual(description_id, 'user-feedback')
        self.assertEqual(description_class, 'form-control')
        self.assertEqual(description_placeholder, 'Put your opinion about this website here')
        self.assertEqual(description_autocomplete, 'off')
