from django.db import models

# Create your models here.
class Feedback(models.Model):
    email = models.EmailField(max_length = 254, null = True)
    feedback_description = models.TextField(max_length = 1000, null = True)

    def __str__(self):
        return '{}'.format(self.email)