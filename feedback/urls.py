from django.urls import path

from . import views

urlpatterns = [
    path('', views.feedback, name = 'feedback'),
    path('collection/', views.feedbackCollection, name = 'feedbackCollection'),
    path('deleteFeedback/<str:pk>/', views.deleteFeedback, name = 'deleteFeedback'),
]