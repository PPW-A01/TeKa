from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from math import floor, ceil

from .forms import MaskForm, PDForm, SanitizeForm, TestForm
from .models import ObediencePercentage

# def index(request):

#     return render(request, 'simulation/simul-form-unfilled.html')

def index(request):

    redirect_page = 'simul-index.html'        
    mask_form = MaskForm()
    pd_form = PDForm()
    sanitize_form = SanitizeForm()
    test_form = TestForm()
    mask_overall = ObediencePercentage.get_ob_overall('MASK')
    pd_overall = ObediencePercentage.get_ob_overall('PD')
    sntz_overall = ObediencePercentage.get_ob_overall('SNTZ')

    context = {
        'test_form':test_form, 
        'mask_form': mask_form, 
        'pd_form':pd_form, 
        'sanitize_form':sanitize_form,
        'mask_percentage':mask_overall,
        'pd_percentage':pd_overall,
        'sanitize_percentage':sntz_overall,
        }
    return render(request, template_name=redirect_page, context=context)

def result(request):
    
    if request.method == 'POST':
        if 'test_simulation' in request.POST :
            test_form = TestForm(request.POST)
            if test_form.is_valid():
                test_count = test_form.cleaned_data['simulated_test']
                new_active_case = retrieve_new_case(test_count)

                # request.session['test_count'] = test_count
                # request.session['new_case'] = new_active_case

                return render(request, 'simul-result.html', context={
                    'test_count':test_count,
                    'new_case':new_active_case
                })
        
    if request.method == 'GET':
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', 'simulation/'))

    redirect_page = 'simul-result.html'
    context = {'test_count':request.session['test_count'], 'new_case':request.session['new_case']}
    return render(request, redirect_page, context)

def score(request):

    if request.method == "POST":
        if 'survey_submission' in request.POST :
            mask_form = MaskForm(request.POST)
            pd_form = PDForm(request.POST)
            sanitize_form = SanitizeForm(request.POST)
            
            if mask_form.is_valid() or pd_form.is_valid() or sanitize_form.is_valid():
                
                mask_type_point = mask_form.cleaned_data['mask_type_point']
                mask_behavior_list = request.POST.getlist('mask_behavior_point')

                pd_exp_point = float(request.POST.get('pd_exp_point'))
                pd_area_crowdedness_point = float(request.POST.get('pd_area_crowdedness_point'))
                pd_area_openness_point = float(request.POST.get('pd_area_openness_point'))

                sanitize_wash_hand_point = float(request.POST.get('sanitize_wash_hand_point'))
                sanitize_soap_type_point = float(request.POST.get('sanitize_soap_type_point'))
                sanitize_wash_time = int(request.POST.get('sanitize_wash_time'))
                sanitize_occasions_list = request.POST.getlist('sanitize_occasions_point')
                sanitize_hsanitizer_point = float(request.POST.get('sanitize_hsanitizer_point'))
                sanitize_alcohol_percentage = int(request.POST.get('sanitize_alcohol_percentage'))
                sanitize_disinfect_point = float(request.POST.get('sanitize_disinfect_point'))

                mask_percentage = process_mask_percentage(mask_type_point, mask_behavior_list)

                pd_percentage = process_pd_percentage(pd_exp_point, pd_area_crowdedness_point, pd_area_openness_point)
                    
                sanitize_percentage = process_sanitize_percentage(
                    sanitize_wash_hand_point,
                    sanitize_soap_type_point,
                    sanitize_wash_time, 
                    sanitize_hsanitizer_point,
                    sanitize_occasions_list,
                    sanitize_alcohol_percentage,
                    sanitize_disinfect_point
                    )

                obedience_percentage_instance = ObediencePercentage.objects.create(mask_obedience=mask_percentage,PD_obedience = pd_percentage,sntz_obedience = sanitize_percentage)

                obedience_percentage_instance.save()

                context = {
                    'mask_percentage': mask_percentage,
                    'pd_percentage': pd_percentage,
                    'sanitize_percentage': sanitize_percentage
                }   

                return render(request, "simul-score.html", context=context)

    else :
        if request.method == 'GET':
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', 'simulation/'))

    redirect_page = 'simul-score.html'
    return render(request, redirect_page)

def process_wash_time_point(wash_time):
    wash_time = int(wash_time)
    if wash_time >= 20 :
        return 1.0
    else :
        return wash_time / 20

def process_alcohol_percentage_point(alcohol_percentage):
    alcohol_percentage = int(alcohol_percentage)
    if alcohol_percentage < 60 :
        return 0.0
    elif alcohol_percentage < 80 :
        return (alcohol_percentage - 60) / 20
    else :
        return 1.0

def process_mask_behavior_point(mask_behavior_list):
    point = 1.0
    for element in mask_behavior_list:
        point += float(element)

    point = round(point, 2)

    if point < 0.0 :
        return 0.0
    else :
        return point

def process_sanitize_occasions_point(sanitize_occasions_list):
    point = 0.0
    for element in sanitize_occasions_list :
        point += float(element)
    return point

def process_mask_percentage(type_point, behavior_list):
    behavior_point = process_mask_behavior_point(behavior_list)
    float_percentage = (type_point + behavior_point) / 2 * 100
    return floor(float_percentage)

def process_pd_percentage(exp_point, crowdedness_point, openness_point):
    float_percentage = (exp_point + crowdedness_point + openness_point) / 2 * 100
    return floor(float_percentage)

def process_sanitize_percentage(wash_hand_point, soap_type_point, wash_time, 
    hsanitizer_point, occassions_list, alcohol_percentage, sanitize_disinfect_point) :

    wash_time_point = process_wash_time_point(wash_time)
    occassions_point = process_sanitize_occasions_point(occassions_list)
    alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)

    if soap_type_point == 0.0 :
        wash_hand_point = 0.0
    
    wash_hand_overall = (soap_type_point + wash_hand_point + wash_time_point) / 3
    hsanitizer_overall = (hsanitizer_point + alcohol_percentage_point) / 2

    float_percentage = (wash_hand_overall + hsanitizer_overall + occassions_point + sanitize_disinfect_point) / 4 * 100
    return floor(float_percentage)

def retrieve_percentage(test_count):
    return 14.0 - test_count / 30000 * 0.1

def retrieve_new_case(test_count):
    percentage = retrieve_percentage(test_count)
    return ceil(percentage / 100 * test_count)