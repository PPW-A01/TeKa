from django import forms
from django.core import validators

class TestForm(forms.Form):
    simulated_test = forms.IntegerField(
        required=True,
        widget=forms.NumberInput(attrs={'class':'forms-simulation-typing-field',
        'placeholder':'test count',
        'min':0,
        'max':200000}),
        validators=[
            validators.MinValueValidator(limit_value=0, message='Minimal value input is 0'),
            validators.MaxValueValidator(limit_value=200000, message='Maximal value input is 200,000')],
    )

MASK_TYPE = [
    (0.0, 'I don\'t usually wear a mask in public.'),
    (0.0, 'Scuba mask (elastic, thin earloop mask made from polyester and spandex)'),
    (0.0, 'Buff mask (elastic, thin neck-covering mask made from cotton and spandex)'),
    (0.0, '1-ply cloth mask'),
    (0.5, '2-ply cloth mask'),
    (0.75, '3-ply cloth mask'),
    (1.0, 'Disposable surgical mask'),
    (1.0, 'N95 mask (5-ply mask with firm structure, usually colored in white)')
]

MASK_BEHAVIOR = [
    (-1.0, 'I don\'t usually wear a mask in public.'),
    (-0.2, 'I wear masks loosely'),
    (-0.2, 'I don\'t really cover my nose or my mouth with my mask'),
    (-0.2, 'I take my mask off for some time in public'),
    (-0.2, 'I push my mask down to my chin or my neck to eat or drink'),
    (-0.2, 'I wear reusable masks multiple times before washing it'),
]

class MaskForm(forms.Form):
    mask_type_point = forms.FloatField(
        required=True,
        label='What kind of mask do you usually wear in public?',
        widget=forms.RadioSelect(choices=MASK_TYPE,
        attrs={'class':'forms-choice'}
        )
    )
    
    mask_behavior_point = forms.FloatField(
        # required=True,
        label='Tell us about your habits when using a mask in public',
        widget=forms.CheckboxSelectMultiple(choices=MASK_BEHAVIOR,
        attrs={'class':'forms-choice'})
    )        

PD_EXP = [
    (0.0, 'I have never been really distancing myself to strangers at least 2 meters (6 ft 6 in) away or paying attention to the distance'),
    (0.5, 'I have tried distancing myself from strangers but I don\'t think it was 2 meters (6 ft 6 in) away or more'),
    (1.0, 'I usually distance myself from strangers in public and I\'m pretty sure that most of the time, the distance has always been at least 2 meters (6 ft 6 in) away')
]

PD_CROWDED_AREA = [
    (0.0, 'Crowded'),
    (0.5, 'Uncrowded')
]

PD_OPEN_AREA = [
    (0.5, 'Open area'),
    (0.0, 'Enclosed space')
]

class PDForm(forms.Form):
    pd_exp_point = forms.FloatField(
        required=True,
        label='Tell us about your experience when conducting physical distancing regarding the COVID-19 pandemic',
        widget=forms.RadioSelect(choices=PD_EXP,
        attrs={'class':'forms-choice'})
    )

    pd_area_crowdedness_point = forms.FloatField(
        required=True,
        label='(1/2) When you go out, in what type of public area do you spend the most time? ',
        widget=forms.RadioSelect(choices=PD_CROWDED_AREA,
        attrs={'class':'forms-choice'})
    )
    
    pd_area_openness_point = forms.FloatField(
        required=True,
        label='(2/2) When you go out, in what type of public area do you spend the most time? ',
        widget=forms.RadioSelect(choices=PD_OPEN_AREA,
        attrs={'class':'forms-choice'})
    )
    

SANITIZE_YES_NO_WASH_HAND = [
    (1.0, 'Yes'),
    (0.0, 'No')
]

SANITIZE_SOAP_TYPE = [
    (0.0, 'No soap'),
    (0.75, 'Bar soap'),
    (1.0, 'Liquid soap')
]

SANITIZE_OCCASIONS = [
    (0.2, 'After coughing or sneezing'),
    (0.2, 'After using the restroom'),
    (0.2, 'Before eating or preparing food'),
    (0.2, 'After contact with animals or pets'),
    (0.2, 'Before touching my face')
]

SANITIZE_YES_NO_H_SANITIZER = [
    (1.0, 'Yes'),
    (0.0, 'No')
]

SANITIZE_YES_NO_DISINFECT = [
    (1.0, 'Yes'),
    (0.0, 'No')
]

class SanitizeForm(forms.Form):
    sanitize_wash_hand_point = forms.FloatField(
        required=True,
        label='Do you wash your hands regularly?',
        widget=forms.RadioSelect(choices=SANITIZE_YES_NO_WASH_HAND,
        attrs={'class':'forms-choice'})
    )
    sanitize_soap_type_point = forms.FloatField(
        required=True,
        label='With which type of soap do you usually wash your hands regularly?',
        widget=forms.RadioSelect(choices=SANITIZE_SOAP_TYPE,
        attrs={'class':'forms-choice'})
    )
    sanitize_wash_time = forms.IntegerField(
        required=True,
        widget=forms.NumberInput(attrs={'class':'forms-survey-typing-field', 
        'placeholder':'...in seconds',
        'min':0,
        'max':120,}), 
        label='How long in seconds do you usually wash your hands',
        validators=[
            validators.MinValueValidator(limit_value=0, message='Minimal value input is 0'),
            validators.MaxValueValidator(limit_value=120, message='Maximal value input is 120')],
    )
    sanitize_occasions_point = forms.FloatField(
        label='In what occasions of these do you usually wash your hands?',
        widget=forms.CheckboxSelectMultiple(choices=SANITIZE_OCCASIONS,
        attrs={'class':'forms-choice'})
    )
    sanitize_hsanitizer_point = forms.FloatField(
        required=True,
        label='If you don\'t have an access to wash your hands, do you usually apply sanitizer to them?',
        widget=forms.RadioSelect(choices=SANITIZE_YES_NO_H_SANITIZER,
        attrs={'class':'forms-choice'})
    )
    sanitize_alcohol_percentage = forms.IntegerField(
        required=True,
        widget=forms.NumberInput(attrs={'class':'forms-survey-typing-field', 
        'placeholder':'...in percent',
        'min':0,
        'max':100}),
        label='What percentage of alcohol does your hand sanitizer contain? (0 if it does not contain alcohol or you don’t usually use hand sanitizer)',
        validators=[
            validators.MinValueValidator(limit_value=0, message='Minimal value input is 0'),
            validators.MaxValueValidator(limit_value=100, message='Maximal value input is 100')],
    )
    sanitize_disinfect_point = forms.FloatField(
        required=True,
        label='When you go home from out, do you usually disinfect carrying, bag or things?',
        widget=forms.RadioSelect(choices=SANITIZE_YES_NO_DISINFECT,
        attrs={'class':'forms-choice'})
    )

# class GoOutNecessitiesForm(forms.Form):
