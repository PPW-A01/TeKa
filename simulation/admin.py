from django.contrib import admin
from .models import ObediencePercentage

# Register your models here.

admin.site.register(ObediencePercentage)
