from django.db import models

# Create your models here.

class ObediencePercentage(models.Model):
    mask_obedience = models.IntegerField()
    PD_obedience = models.IntegerField()
    sntz_obedience = models.IntegerField()

    @classmethod
    def get_ob_overall(self, ob_type):
        ob_overall_sum_float = 0.0
        ob_percentage_list = self.objects.all()
        ob_percentage_count = ob_percentage_list.count()

        if ob_type == 'MASK' :
            for obp in ob_percentage_list:
                ob_overall_sum_float += obp.mask_obedience
        
        elif ob_type == 'PD':
            for obp in ob_percentage_list:
                ob_overall_sum_float += obp.PD_obedience

        elif ob_type == 'SNTZ' :
            for obp in ob_percentage_list:
                ob_overall_sum_float += obp.sntz_obedience

        try:
            ob_overall_mean = ob_overall_sum_float / ob_percentage_count

        except ZeroDivisionError:
            ob_overall_mean = 0

        return str('%.1f' %  ob_overall_mean)
