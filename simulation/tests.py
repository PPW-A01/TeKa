from django.test import TestCase, Client
from django.urls import resolve

from .views import *
from .models import *

# Create your tests here.

class SimulationGeneralTesting(TestCase):
    def test_simulation_url_exists(self):
        response = Client().get('/simulation/')
        self.assertEquals(response.status_code, 200)

    def test_simulation_result_url_exists(self):
        response = Client().post('/simulation/', {'simulated_test': '20000'} )
        self.assertEquals(response.status_code, 200)

    def test_simulation_score_url_exists(self):
        response = Client().post('/simulation/', {
            'mask_type_point': 1.0,
            'mask_behavior_point': [],
            'pd_exp_point': 1.0,
            'pd_area_crowdedness_point':0.5,
            'pd_area_openness_point':0.5,
            'sanitize_wash_hand_point':1.0,
            'sanitize_soap_type_point':1.0,
            'sanitize_wash_time':30,
            'sanitize_occasions_point':[0.2, 0.2, 0.2, 0.2, 0.2],
            'sanitize_hsanitizer_point':1.0,
            'sanitize_alcohol_percentage':80,
            'sanitize_disinfect_point':1.0
            })
        self.assertEquals(response.status_code, 200)

    def test_simulation_uses_index_template(self):
        response = Client().get('/simulation/')
        self.assertTemplateUsed(response, 'simul-index.html')
        
    def test_simulation_result_uses_result_template(self):
        response = Client().post('/simulation/', {'simulated_test': '20000'} )
        self.assertTemplateUsed(response, 'simul-index.html')  

    def test_simulation_score_uses_score_template(self):
        response = Client().post('/simulation/', {
            'mask_type_point': 1.0,
            'mask_behavior_point': [],
            'pd_exp_point': 1.0,
            'pd_area_crowdedness_point':0.5,
            'pd_area_openness_point':0.5,
            'sanitize_wash_hand_point':1.0,
            'sanitize_soap_type_point':1.0,
            'sanitize_wash_time':30,
            'sanitize_occasions_point':[0.2, 0.2, 0.2, 0.2, 0.2],
            'sanitize_hsanitizer_point':1.0,
            'sanitize_alcohol_percentage':80,
            'sanitize_disinfect_point':1.0
            })
        self.assertTemplateUsed(response, 'simul-index.html')

    def test_simulation_uses_index_func(self):
        found = resolve('/simulation/')
        self.assertEquals(found.func, index)
        # pass
    
    def test_simulation_uses_result_func(self):
        found = resolve('/simulation/result/')
        self.assertEquals(found.func, result)

    def test_simulation_uses_score_func(self):
        found = resolve('/simulation/score/')
        self.assertEquals(found.func, score)

    def test_form_is_mask_form_valid(self):
        form_data = {
            'mask_type_point': 1.0,
            'mask_behavior_point': 0.0
            }
        form = MaskForm(data=form_data)
        self.assertTrue(form.is_valid())

        mask_type_point = form.data['mask_type_point']
        self.assertEquals(mask_type_point, 1.0)

    def test_form_is_pd_form_valid(self):
        form_data = {
            'pd_exp_point': 1.0,
            'pd_area_crowdedness_point': 0.5,
            'pd_area_openness_point':0.5
            }
        form = PDForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_form_is_sntz_form_valid(self):
        form_data = {
            'sanitize_wash_hand_point': 1.0,
            'sanitize_soap_type_point': 1.0,
            'sanitize_wash_time':20,
            'sanitize_occasions_point': 1.0,
            'sanitize_hsanitizer_point': 1.0,
            'sanitize_alcohol_percentage': 80,
            'sanitize_disinfect_point':1.0
            }
        form = SanitizeForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_model_get_overall(self):
        ObediencePercentage.objects.create(mask_obedience=100, PD_obedience=20, sntz_obedience=50)
        ObediencePercentage.objects.create(mask_obedience=0, PD_obedience=40, sntz_obedience=80)

        mask_percentage = ObediencePercentage.get_ob_overall('MASK')
        self.assertEquals(mask_percentage, '50.0')

        pd_percentage = ObediencePercentage.get_ob_overall('PD')
        self.assertEquals(pd_percentage, '30.0')

        sntz_percentage = ObediencePercentage.get_ob_overall('SNTZ')
        self.assertEquals(sntz_percentage, '65.0')

    def test_model_can_create_new_percentage(self):
        ObediencePercentage.objects.create(mask_obedience=1, PD_obedience=1, sntz_obedience=1)
        OP_count = ObediencePercentage.objects.all().count()
        self.assertEquals(OP_count, 1)

    def test_views_process_wash_time_point_works(self):
        wash_time = '30'
        wash_time_point = process_wash_time_point(wash_time)
        self.assertEquals(wash_time_point, 1.0)

        wash_time = '10'
        wash_time_point = process_wash_time_point(wash_time)
        self.assertEquals(wash_time_point, 0.5)
    
    def test_views_process_alcohol_percentage_point_works(self):
        alcohol_percentage = 50
        alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)
        self.assertEquals(alcohol_percentage_point, 0.0)

        alcohol_percentage = 70
        alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)
        self.assertEquals(alcohol_percentage_point, 0.5)

        alcohol_percentage = 90
        alcohol_percentage_point = process_alcohol_percentage_point(alcohol_percentage)
        self.assertEquals(alcohol_percentage_point, 1.0)


    def test_views_process_mask_behavior_point_works(self):
        mask_behavior = [-1.0]
        mask_behavior_point = process_mask_behavior_point(mask_behavior)
        self.assertEquals(mask_behavior_point, 0.0)

        mask_behavior = [-1.0, -0.2, -0.2]
        mask_behavior_point = process_mask_behavior_point(mask_behavior)
        self.assertEquals(mask_behavior_point, 0.0)

        mask_behavior = [-0.2, -0.2, -0.2]
        mask_behavior_point = float(process_mask_behavior_point(mask_behavior))
        self.assertEquals(mask_behavior_point, 0.4)

        mask_behavior = []
        mask_behavior_point = process_mask_behavior_point(mask_behavior)
        self.assertEquals(mask_behavior_point, 1.0)
    
    
    def test_views_process_sanitize_occasions_point_works(self):
        sanitize_occasions_list = []
        sanitize_occasions_point = process_sanitize_occasions_point(sanitize_occasions_list)
        self.assertEquals(sanitize_occasions_point, 0.0)

        sanitize_occasions_list = [0.2, 0.2]
        sanitize_occasions_point = process_sanitize_occasions_point(sanitize_occasions_list)
        self.assertEquals(sanitize_occasions_point, 0.4)

        sanitize_occasions_list = [0.2, 0.2, 0.2, 0.2, 0.2]
        sanitize_occasions_point = process_sanitize_occasions_point(sanitize_occasions_list)
        self.assertEquals(sanitize_occasions_point, 1.0)

    def test_views_process_mask_percentage_works(self):
        type_point = 1.0
        behavior_list = [-1.0]
        mask_percentage = process_mask_percentage(type_point, behavior_list)
        self.assertEquals(mask_percentage, 50)

        type_point = 1.0
        behavior_list = [-0.2, -0.2]
        mask_percentage = process_mask_percentage(type_point, behavior_list)
        self.assertEquals(mask_percentage, 80)

        type_point = 0.5
        behavior_list = [-0.2, -0.2, -0.2, -0.2]
        mask_percentage = process_mask_percentage(type_point, behavior_list)
        self.assertEquals(mask_percentage, 35)

    def test_views_process_pd_percentage_works(self):
        exp_point = 1.0
        crowdedness_point = 0.5
        openness_point = 0.5
        pd_percentage = process_pd_percentage(exp_point, crowdedness_point, openness_point)
        self.assertEquals(pd_percentage, 100)

        exp_point = 1.0
        crowdedness_point = 0.5
        openness_point = 0.0
        pd_percentage = process_pd_percentage(exp_point, crowdedness_point, openness_point)
        self.assertEquals(pd_percentage, 75)

        exp_point = 0.0
        crowdedness_point = 0.5
        openness_point = 0.5
        pd_percentage = process_pd_percentage(exp_point, crowdedness_point, openness_point)
        self.assertEquals(pd_percentage, 50)

    def test_views_process_sntz_percentage_works(self):
        wash_hand_point = 1.0
        soap_type_point = 1.0
        wash_time = 20
        hsanitizer_point = 1.0
        occassions_list = [0.2, 0.2, 0.2, 0.2, 0.2]
        alcohol_percentage = 80
        sanitize_disinfect_point = 1.0
        sntz_percentage = process_sanitize_percentage(wash_hand_point, soap_type_point, wash_time, 
            hsanitizer_point, occassions_list, alcohol_percentage, sanitize_disinfect_point)    
        self.assertEquals(sntz_percentage, 100)

        wash_hand_point = 0.0
        soap_type_point = 0.0
        wash_time = 10
        hsanitizer_point = 0.0
        occassions_list = [0.2, 0.2, 0.2]
        alcohol_percentage = 70
        sanitize_disinfect_point = 0.0
        sntz_percentage = process_sanitize_percentage(wash_hand_point, soap_type_point, wash_time, 
            hsanitizer_point, occassions_list, alcohol_percentage, sanitize_disinfect_point)
        self.assertEquals(sntz_percentage, 25)
    
    def test_simulation_retrieve_new_case_works(self):
        test_count = 10000
        new_case = retrieve_new_case(test_count)
        self.assertEquals(new_case, 1397)

        test_count = 100000
        new_case = retrieve_new_case(test_count)
        self.assertEquals(new_case, 13667)