# Generated by Django 3.1.2 on 2020-11-14 16:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simulation', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='obediencepercentage',
            name='GON_obedence',
        ),
    ]
