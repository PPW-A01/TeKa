from django.urls import path

from . import views

app_name = 'simulation'

urlpatterns = [
    path('', views.index, name='simulation'),
    path('result/', views.result, name='simulation-result'),
    path('score/', views.score, name='simulation-score'),
]