# NCovid

[![Pipeline](https://gitlab.com/PPW-A01/TeKa/badges/master/pipeline.svg)](https://gitlab.com/PPW-A01/TeKa/pipelines)
[![Coverage](https://gitlab.com/PPW-A01/TeKa/badges/master/coverage.svg)](https://gitlab.com/PPW-A01/TeKa/pipelines)


Perkenalkan kami mahasiswa Fakultas Ilmu Komputer Universitas Indonesia, akan mendokumentasikan tahap-tahap yang kami lakukan selama menyelesaikan tugas mata kuliah Perancangan dan Pemrograman Web.

Nama Kelompok       : A01  
Anggota Kelompok    :  
    1. Danan Maulidan Akbar - 1906293000  
    2. Muhammad Faisal Adi Soesatyo - 1906293184  
    3. Muhammad Irfan Junaidi - 1906293202  
    4. Nadilatusifa - 1906293240  

Link Herokuapp kami : https://teka1.herokuapp.com/  

## Overview  
Tema yang kami angkat untuk website kami adalah seputar Covid-19. Ide dasar dari website ini adalah kami ingin memberikan warna berbeda dalam menyampaikan informasi terkini terkait Covid-19 di Indonesia. Informasi yang kami sajikan dalam website ini, kami kemas dalam bentuk visual, data, maupun tulisan. Sehingga pengunjung selain mendapatkan informasi terkini terkait Covid-19 juga merasakan pengalaman yang unik ketika mengunjunginya. Kami harap website ini dapat menumbuhkan rasa kesadaran diri akan pentingnya menjaga kesehatan diri dan juga rasa peduli akan sesama.

## Features  
1. Peta Persebaran Covid-19 di tiap provinsi di Indonesia  
2. _Active Cases Simulator_  
3. Tips-Tips untuk menjaga diri dan lingkungan dari virus corona  
4. Feedback terkait penyajian informasi di dalam website kami  

